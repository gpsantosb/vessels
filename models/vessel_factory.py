"""Vessel Factory Module: get vessel information from different sources."""

import datetime
import string
from abc import ABC
import requests
from requests_html import HTMLSession
from bs4 import BeautifulSoup
from pymongo import MongoClient
from models.vessel import Vessel
from utils.context import AppContext

class VesselFactory(ABC):
    def find_vessel(self, key):
        raise NotImplementedError('Method not implemented')

class VesselStore(ABC):
    def store_vessels(self, vessel):
        raise NotImplementedError('Method not implemented')

class VesselFinderVesselFactory(VesselFactory):
    CATEGORIES = {
            "General / Dry cargo": 'generic',
            "Bulk carrier": 'solidBulk',
            "Container Ship": 'containers',
            "Container / Reefer": 'containers',
            "Tanker": 'liquidBulk',
            "LNG / LPG / CO2 Tanker": 'liquidBulk',
            "Chemical Tanker": 'liquidBuld',
            "Oil Tanker": 'liquidBuld',
            "Passenger / Cruise": 'passengersCruise',
            "High speed craft": 'passengersQuick',
            "Yacht / Pleasure craft": 'yacht',
            "Fishing": 'fishing',
            "Offshore": 'deepSeaSupport',
            "Military": 'warship',
            "Auxiliary": 'other',
            "Other / Unknown type": 'generic'
        }
    SERVER = 'https://www.vesselfinder.com'
    QUERY = '{}/vessels?name={}'

    @classmethod
    def __remove_units(cls, value):
        return value.rstrip(string.ascii_letters + string.whitespace)

    @classmethod
    def __get_category(cls, value):
        category = cls.CATEGORIES.get(value)
        if category is None:
            AppContext.logger().warning('Category not found <%s>', value)
            category = 'generic'
        return category

    @classmethod
    def __create_vessel(cls, data):
        vessel = Vessel()
        vessel._callsign = data.get('Callsign')
        vessel._dwt = data.get('Summer Deadweight (t)')
        vessel._draft = cls.__remove_units(data.get('Current draught'))
        vessel._flag = data.get('Flag')
        vessel._gt = data.get('Gross Tonnage')
        vessel._imo = data.get('IMO number')
        vessel._lastupdated = datetime.datetime.utcnow()
        vessel._length, vessel._width = cls.__remove_units(data.get('Length / Beam')).split(' / ')
        vessel._mmsi = data.get('IMO / MMSI').split(' / ')[1]
        vessel._name = data.get('Vessel Name')
        vessel._type = cls.__get_category(data.get('Ship type'))
        return vessel

    def find_vessel(self, key):
        result = None
        url = self.QUERY.format(self.SERVER, key)
        session = HTMLSession()
        page = session.get(url)
        ships = page.html.find('a.ship-link')

        for ship in ships:
            ship_page = session.get(self.SERVER + ship.attrs['href'])
            #ship_page.html.render()
            rows = ship_page.html.find('tr')
            element = {}
            element['Vessel Name'] = ship_page.html.find('h1.title')[0].text
            for row in rows:
                data = row.find('td')
                if len(data) == 2:
                    element[data[0].text] = data[1].text
            result = self.__create_vessel(element)
            break # return the first one

        return result

class MongoVesselFactory(VesselFactory, VesselStore):
    def __init__(self):
        config = AppContext.config()
        url = 'mongodb://{}:{}@{}:{}'.format(config['MONGO_USER'], config['MONGO_PASS'], config['MONGO_URL'], config['MONGO_PORT'])
        AppContext.logger().info('Connecting to MongoDB <%s>', url)
        self._db = MongoClient(url).vessels

    def find_vessel(self, key):
        query = {'$or': [{'mmsi': key}, {'imo': key }]}
        doc = self._db.vessels.find_one(query)
        return None if doc is None else self.__create_vessel(doc)

    def store_vessel(self, vessel):
        doc = self._db.vessels.find_one({'_id': vessel.id})
        if doc == None:
            obj = vessel.serialize().pop('id')
            self._db.vessels.insert_one(obj)

    @classmethod
    def __create_vessel(cls, doc):
        vessel = Vessel()
        vessel.from_dictionary(doc.items())
        return vessel

__factories = {
        'mongodb': lambda: MongoVesselFactory(),
        'vesselfinder': lambda: VesselFinderVesselFactory()
    }

def get_factory(key):
    return __factories[key]()
