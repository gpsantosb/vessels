"""Vessel class"""
from typing import Dict
from utils.serializer import Serializable

class Vessel(Serializable):
    def __init__(self):
        self._id = ""
        self._callsign = ""
        self._dwt = 0
        self._draft = 0
        self._flag = ""
        self._gt = 0
        self._imo = ""
        self._length = 0
        self._mmsi = ""
        self._name = ""
        self._type = 0
        self._width = 0

    def from_dictionary(self, dictionary):
        for key, value in dictionary:
            final_key = key if key.startswith('_') else '_{}'.format(key)
            if hasattr(self, final_key):
                setattr(self, final_key, value)

    @property
    def id(self):
        return self._id

    def serialize(self) -> Dict:
        return {
            'id': self._id,
            'callsign': self._callsign,
            'draft': self._draft,
            'dwt': self._dwt,
            'flag': self._flag,
            'gt': self._gt,
            'imo': self._imo,
            "length": self._length,
            'mmsi': self._mmsi,
            'name': self._name,
            "type": self._type,
            "width": self._width
        }
