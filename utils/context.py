from flask import Flask

class AppContext(object):
    _app = None

    def __init__(self):
        raise NotImplementedError('call instance() instead')

    @classmethod
    def set_app(cls, webapp):
        cls._app = webapp

    @classmethod
    def app(cls):
        return cls._app
    
    @classmethod
    def logger(cls):
        return cls.app().logger

    @classmethod
    def config(cls):
        return cls.app().config