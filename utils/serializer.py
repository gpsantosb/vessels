from abc import ABC
from typing import Dict
from bson.objectid import ObjectId
import datetime
from json import JSONEncoder

class Serializable(ABC):
    def serialize(self) -> Dict:
        return Dict()

class Serializer(JSONEncoder):
    def default(self, o):
        if isinstance(o, Serializable):
            return o.serialize()
        
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()

        if isinstance(o, ObjectId):
            return str(o)
        
        return super(Serializer, self).default(o)