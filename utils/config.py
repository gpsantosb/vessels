__configurations = {
    'DEV': lambda: ConfigDev()
}

def get_configuration(env):
    return __configurations.get(env, lambda: Config())()

class Config(object):
    def __init__(self):
        self.DEBUG = False
        self.TEST = False
        self.MONGO_URL = 'mongodb'
        self.MONGO_PORT = 27017
        self.MONGO_USER = 'root'
        self.MONGO_PASS = 'admin'
        self.FACTORIES = ['vesselfinder']
        self.STORES = []
        #self.STORES = ['mongodb']

    def print(self):
        for key in dir(self):
            if key.isupper(): 
                print('{} = {}'.format(key, getattr(self, key)))

class ConfigDev(Config):
    def __init__(self):
        super().__init__()
        self.DEBUG = True
        self.TEST = True
        self.MONGO_URL = 'locaalhost'