#!/usr/bin/env python3

from flask import Flask
from flask_restful import Api
from controllers.vessel_controller import VesselController
from utils.serializer import Serializer
from utils.context import AppContext
from utils.config import get_configuration

app = Flask(__name__)

AppContext.set_app(app)
config = get_configuration('DEV')
config.print()

api = Api(app)
api.add_resource(VesselController, '/vessel')

app.json_encoder = Serializer
app.config.from_object(config)
#app.run(host='0.0.0.0')
app.debug = True
app.run()
