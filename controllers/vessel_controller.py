"""Vessel Controller module to handle API requests"""

from flask import jsonify, request
from flask_restful import Resource
from models.vessel import Vessel
from models.vessel_factory import get_factory
from utils.context import AppContext

class VesselController(Resource):
    def __init__(self):
        self._factories = []
        for factory in AppContext.config()['FACTORIES']:
            AppContext.logger().info('Added factory <%s>', factory)
            self._factories.append(get_factory(factory))

        self._stores = []
        for store in AppContext.config()['STORES']:
            AppContext.logger().info('Added store <%s>', store)
            self._stores.append(get_factory(store))


    def get(self):
        print(request)
        query = request.args.get('query')

        result = None
        for factory in self._factories:
            vessel = factory.find_vessel(query)
            AppContext.logger().info('Result using factory <%s>: <%s>', factory, vessel)
            if isinstance(vessel, Vessel):
                result = vessel
                break
        
        if result != None:
            for store in self._stores:
                store.store_vessel(result)

        return jsonify(result)
