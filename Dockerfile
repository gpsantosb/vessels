# Dockerfile - this is a comment. Delete me if you want.
FROM python:3-alpine

COPY . /app
WORKDIR /app
RUN pip3 install flask flask_restful requests beautifulsoup4 pymongo
ENTRYPOINT ["python"]
EXPOSE 5000

CMD ["app.py"]